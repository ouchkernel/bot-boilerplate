package main

import (
	"log"
	"os"
	"strconv"

	"github.com/bwmarrin/discordgo"
)

func main() {
	prefix := os.Getenv("BOT_PREFIX")
	ll := 1
	s, err := discordgo.New("Bot " + os.Getenv("BOT_TOKEN"))

	if os.Getenv("LOG_LEVEL") == "" {
		ll, _ = strconv.Atoi(os.Getenv("LOG_LEVEL"))
	}
	s.LogLevel = ll
	if err != nil {
		log.Fatal(err)
	}

	router := newRouter()
	// Add message handler
	s.AddHandler(func(_ *discordgo.Session, m *discordgo.MessageCreate) {
		router.FindAndExecute(s, prefix, s.State.User.ID, m.Message)
	})

	err = s.Open()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("bot is running...")
	// Prevent the bot from exiting
	<-make(chan struct{})
}
