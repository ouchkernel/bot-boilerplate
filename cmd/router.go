package main

import (
	"github.com/Necroforger/dgrouter/exrouter"
	"gitlab.com/ouchkernel/bot-boilerplate/sample"
)

func newRouter() (r *exrouter.Route) {
	r = exrouter.New()

	r.Group(func(rt *exrouter.Route) {
		rt.Cat("sample")
		rt.On("sample", sample.SetMute).Desc("Mutes user")
	})
	r.Default = r.On("help", func(ctx *exrouter.Context) {
		var text = ""
		for _, v := range r.Routes {
			text += v.Name + " : \t" + v.Description + "\n"
		}
		ctx.Reply("```" + text + "```")
	}).Desc("prints this help menu")
	return r
}
